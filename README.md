# Examen 1702. Recuperación Java EE

## Base de datos

- Ejecuta el sql que encontrarás en el repositorio. Este sql crea una base de datos llamada examen1702.
- Todo el examen versa sobre la tabla posts

## Ejercicios

1. Ruta '/post/index'. Lista de posts con las columnas: id, autor, titulo. 2Puntos
2. Pagina la anterior vista. 2 Puntos.
3. Crea una vista con el detalle de un post. Ruta '/post/view/{id}'. 2Puntos.
4. Crea un formulario de alta de posts y su procesamiento. Rutas '/posts/create' y '/post/store'. 2 Puntos.
5. Cada vez que se visita un post debe guardarse el id de dicho post. La ruta '/post/last' debe buscar
el post guardado y mostrar la ruta '/posts/view/{id}. 2 Puntos.