-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2017 at 12:12 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examen1701`
--
CREATE DATABASE IF NOT EXISTS `examen1702` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examen1702`;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Eduardo Redondo', 'Provident beatae deleniti facilis soluta laboriosam beatae quo.', 'Similique cumque rerum molestias quia voluptatum et mollitia. Tempore nam aut eveniet molestiae odio. Cumque ut labore a omnis nostrum corporis. Nihil repellat id labore harum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(2, 'Francisco Luján Segundo', 'Quo eos fugiat beatae eveniet.', 'Aut sed voluptatem fugiat sed incidunt. Nisi non distinctio quis velit recusandae vero. Eum perferendis sed et neque quasi nisi. Magnam ut blanditiis vitae consequuntur deserunt eveniet a quidem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(3, 'Joan Carbonell', 'Quidem molestias et est dolores neque at sunt.', 'Consequuntur praesentium voluptas quisquam iusto inventore. Ab ea aliquid iure rerum sunt. Commodi aperiam itaque nemo error dolor sunt. Quisquam non nihil quisquam quod quia aliquam minus.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(4, 'Nerea Cervantes Hijo', 'Est exercitationem deleniti et eum.', 'Dolorem rerum nihil qui fuga voluptatem pariatur magnam. Culpa labore aperiam temporibus fugit soluta deleniti illum. Officia doloremque saepe eos provident unde possimus consequatur. Sed quia sunt harum nihil.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(5, 'Yeray Bustamante', 'Id et aut sunt occaecati aut omnis.', 'Fugiat debitis reiciendis et labore dicta ipsum. Doloremque voluptas ea nihil ipsa quos. Recusandae eos dolorem et dolorum eos.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(6, 'Alba Villarreal', 'Ad cumque molestiae dolor.', 'Sint quia quia velit voluptas doloribus quia ut omnis. Facilis veniam iusto architecto. Porro alias incidunt distinctio et dolorem. Ut et asperiores doloribus aspernatur aut voluptas. Beatae temporibus sint nihil nihil facere doloremque voluptas non.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(7, 'Ian Tijerina', 'Explicabo culpa perferendis molestiae fuga distinctio pariatur.', 'Et quia ducimus accusamus. Veritatis qui ex occaecati est consequatur id voluptatum. Voluptas quos quidem quia inventore nihil.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(8, 'Ing. Marina Navarrete Segundo', 'Pariatur ad et sit voluptatibus et.', 'Est dolorem rem animi repudiandae iusto praesentium. Aperiam sunt beatae maiores vitae voluptatem consequatur laboriosam dolor.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(9, 'Srita. Vera Trejo Tercero', 'Aspernatur aut quia rem adipisci modi velit.', 'Eos soluta ratione ad consequuntur et officiis. Ipsa necessitatibus eius voluptatum sed fuga. Quas reprehenderit nostrum ullam qui quas saepe cum. Ex quia praesentium rem consequatur dolorum nemo maiores.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(10, 'Cristian Segura Tercero', 'Non quo dolore voluptas vero consequatur eum omnis rerum.', 'Officia et et molestias neque rem. Omnis in accusantium placeat quos dolores illo suscipit incidunt. Expedita cupiditate qui ducimus unde quia qui.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(11, 'Ing. Ian Quiñónez', 'Sunt alias qui dolorum eveniet vel cupiditate aut.', 'Qui quis vero alias veniam voluptatum architecto. Aut repellat dolor est sint vel eaque. Alias officia neque maiores itaque. Ea assumenda odio natus fugit tempore iste minima ullam.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(12, 'Guillem Cobo Segundo', 'Rerum dolores amet dolorem magni ratione ut.', 'Velit molestias eum similique consequuntur cum velit. In doloribus non enim et eligendi omnis. Quis et est vel nam. Officiis sunt nulla fugit perspiciatis at dolore aut velit.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(13, 'Erika Olivares', 'Totam eum rem consequatur explicabo.', 'Odit cupiditate ea omnis. Possimus vel nihil provident. Reprehenderit molestias ut aliquid mollitia.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(14, 'Cesar Ballesteros', 'Explicabo ut voluptas harum eius.', 'Voluptatum ut vel omnis explicabo consequuntur. Illo animi error natus ut veniam eaque.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(15, 'Rocio Garza', 'Ut ad non dolorem qui vel consequatur.', 'Corrupti quo atque assumenda. Ad nobis et voluptatum quod laudantium. Vero exercitationem ducimus iure expedita ut reprehenderit rem. Error dicta dolor architecto labore.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(16, 'Alejandra Saldaña', 'Ad voluptate ut sunt odit ut doloribus.', 'Dicta qui repellendus ut maiores minima iusto repellat. Itaque voluptatum fugiat consequatur occaecati molestiae eveniet. Ad est rerum molestiae quam. Laudantium dolorem eos magnam unde adipisci est fugit.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(17, 'Leire Abad Hijo', 'Nostrum occaecati quis voluptatem dignissimos animi eos velit.', 'Minus vero omnis architecto voluptate. Reiciendis eum quis atque tempore. Fugiat qui consequatur voluptatem non excepturi voluptatem. Modi omnis rerum eveniet molestias.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(18, 'Dr. Anna Sisneros Tercero', 'Dolorem molestiae nihil magnam.', 'Et neque reprehenderit voluptatem fugit dicta sint velit. Aut ipsam officiis soluta perferendis occaecati tenetur. Voluptatum numquam quaerat iste et soluta eligendi nesciunt inventore. Occaecati quas commodi mollitia facere.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(19, 'Laia De la fuente', 'Architecto autem ipsa et harum dignissimos vitae.', 'Recusandae dolore eum sit quas sed sit minus. Error et saepe consequatur voluptatem. Fuga ad excepturi similique error consectetur. Amet aliquid qui libero et.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(20, 'Leo Deleón', 'Quis sit excepturi est sint ipsum deleniti vel rerum.', 'Dignissimos doloribus pariatur odio et et aliquam. Dolores at consequatur officiis deleniti et quis. Exercitationem inventore et quidem perferendis ipsum delectus.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(21, 'Dn. Jan Rascón Hijo', 'Labore ut est veritatis sunt quos et repellat veniam.', 'Mollitia ut nemo libero inventore asperiores voluptas et. Quia molestiae quia consequatur.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(22, 'Yeray Santamaría', 'Neque et saepe eius voluptas minima.', 'Commodi atque qui reprehenderit perferendis debitis rerum corrupti. Inventore nostrum voluptatum perspiciatis repudiandae maxime fuga blanditiis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(23, 'Dr. Gael Domingo', 'Repellendus dolorem qui amet vero ut.', 'Ratione aut accusamus corrupti maxime. Qui iure ut sequi ratione consequatur hic. Minima mollitia qui repudiandae quos numquam velit voluptas.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(24, 'Lic. Roberto Elizondo', 'Quam laudantium dolorum impedit quia qui.', 'Iure quos et amet reprehenderit dignissimos dolorum. Enim quas et assumenda in soluta error perferendis. Minima et doloribus maxime quidem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(25, 'Aitor Partida Hijo', 'Consequuntur dignissimos qui soluta ipsa.', 'Amet inventore accusantium saepe expedita id est accusantium. Et eos autem nemo quis animi et. Sed sapiente officia quia expedita quaerat sint veniam totam. Ad molestias labore commodi quod.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(26, 'Ian Sáenz Segundo', 'Perferendis quisquam cupiditate non.', 'Qui dolorum dolorem odio nam voluptatem. Nulla dolor perferendis quia commodi.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(27, 'Iker Palomo', 'At natus non doloremque.', 'Sit aspernatur consequuntur et tenetur culpa rerum veniam. Rerum quis ullam et cupiditate delectus magni aut. Deleniti ipsa voluptatibus voluptatem nihil.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(28, 'Dario Longoria', 'Ut fugiat delectus tenetur vitae natus aut a.', 'Ea recusandae temporibus sequi voluptas. Doloribus nostrum fuga maxime eum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(29, 'Srita. Fatima Torres', 'Eum aperiam nostrum exercitationem molestias nam blanditiis.', 'Rerum ut nostrum quo maxime ab. Quia magnam quo eum sit corrupti. Vel ut ab placeat accusantium sapiente repudiandae. Dolores iste nobis vel cum. Nemo reiciendis qui iusto aut in.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(30, 'Raul Sola Hijo', 'Odit omnis et esse recusandae.', 'Excepturi enim ratione et quae. Mollitia dolore qui ut. Maiores enim dolor quae asperiores. Expedita incidunt occaecati neque ipsa omnis ducimus.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(31, 'Joan Delatorre', 'Voluptates eos magni similique expedita.', 'Porro ut blanditiis quas sed ut nihil accusantium perferendis. Repellat excepturi assumenda laborum impedit perspiciatis molestiae est facere. Natus nulla quidem quis id ea. Rerum velit corrupti suscipit. Aspernatur atque ab mollitia debitis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(32, 'Joel Peres', 'Quas accusantium iusto nihil repudiandae sunt.', 'Veritatis fugiat ipsum sunt a aut. Aut ut minima in laborum rerum dolor quam. Possimus hic dolores nam quasi laudantium nemo voluptas ratione.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(33, 'Ariadna Jasso Hijo', 'Omnis minima deserunt quas dolorum quis.', 'Cum maxime officia minus sint et aut. Vel doloremque illum id qui voluptatibus. Ut quam sunt suscipit ratione aut unde. Debitis aut veritatis tenetur ipsa dolorem aut.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(34, 'Sr. Leo Alvarado', 'Placeat eum repudiandae doloribus molestias in vel sunt.', 'Fugit voluptate unde ut aut rerum quisquam. Perferendis recusandae quisquam repudiandae id. Vel sit expedita placeat ea rerum ut quas. Distinctio perferendis aut animi dicta fuga suscipit.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(35, 'Berta Conde', 'Tempora deleniti ipsum tenetur saepe corporis.', 'Quia vitae dolorem quia delectus. Est id cumque doloribus voluptas aut ea. Totam eveniet occaecati consectetur.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(36, 'Julia De la torre', 'Incidunt aut molestias illo fugiat.', 'Qui minima saepe id nisi distinctio. Fuga distinctio quidem dolor eos et et odio. Praesentium ut consequatur fugit totam nihil libero.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(37, 'Pablo Ojeda', 'Eaque voluptas est nam error.', 'A ex aut non nesciunt et perferendis modi voluptatem. Expedita harum itaque saepe nam omnis aut cupiditate. Voluptas quaerat et quibusdam corrupti. Libero aliquam optio labore quia autem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(38, 'Helena Ballesteros', 'Velit labore nesciunt ipsa temporibus amet recusandae.', 'Eius et qui aut qui. Quidem nemo quam omnis enim. Voluptates ipsa aut vero magni illo. Suscipit quidem omnis ducimus mollitia accusamus aspernatur.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(39, 'Alma Marín', 'Libero aperiam aut voluptatibus quos commodi qui ut.', 'Deserunt aut nihil voluptates eligendi quidem. Totam temporibus ipsam ea dicta reiciendis sit dolor id.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(40, 'Gabriel Soriano', 'Voluptas eveniet facere repellendus cupiditate eos aut quis.', 'Expedita et fugit libero nihil commodi repellat. Sit culpa magnam fugiat ipsam. Omnis quaerat cum architecto et nisi ab similique. At quia voluptatum vel autem atque illum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(41, 'Manuel Aguilera', 'Et totam nemo vel sequi.', 'Porro quas ut incidunt nostrum et ipsum. Eos nihil odit illum consectetur molestiae. Ad quis est aut consectetur illum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(42, 'Ing. Guillermo Sierra', 'Incidunt est temporibus eaque aliquid qui temporibus aperiam.', 'Qui sit tempore optio. Qui nobis voluptates ea pariatur ut voluptas ut. Quia est non nobis expedita possimus eum sed. Quae voluptatum commodi est qui alias quasi et eius.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(43, 'Alonso Lebrón Tercero', 'Non temporibus nostrum quo commodi error.', 'Tempora porro quia voluptas a. Molestias dolores animi rerum ab necessitatibus dolore. Quaerat voluptatibus autem molestias quo.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(44, 'Pedro Arellano', 'Autem et rerum neque recusandae et consequuntur quaerat esse.', 'Ut nesciunt nemo officiis consequatur non aut aut. Aut qui consequatur eum animi unde quam. Suscipit consequatur laboriosam aut aliquam. Consequuntur aut et minus ipsum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(45, 'Ing. Naiara Simón Tercero', 'Voluptate expedita voluptatem non consequatur amet.', 'Eos incidunt accusantium optio mollitia cum sequi atque. Nesciunt natus est similique et. Est quidem velit sed omnis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(46, 'Sr. Dario Frías', 'Autem itaque in quia neque.', 'Debitis sunt ut ut provident et ut. Est voluptas laborum et fuga qui dolores suscipit.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(47, 'Dn. Jorge Fonseca Hijo', 'Odio nihil vel corrupti.', 'Et modi ipsum reprehenderit tempora totam placeat. Ipsum ratione voluptates dolorem et voluptas occaecati. Ut facilis ullam quia. Et nisi quis quis rerum aliquid officiis recusandae.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(48, 'Biel Soriano', 'Officiis est eligendi aut.', 'Et ut dolor nam sed. Laudantium animi repellendus corporis culpa. Voluptatem omnis sed dolore perspiciatis dolorem et exercitationem. Doloribus nisi sit eum velit maiores.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(49, 'Sandra Cuellar', 'Et est ex quis consequatur molestiae quidem.', 'Neque in hic quod quibusdam fugit. Iure et sit illum facere amet consequatur consequuntur.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(50, 'Dr. Rodrigo Castellano', 'Repellendus non placeat consectetur sunt corrupti.', 'Voluptates molestias reiciendis tempora vitae et unde non. Et non adipisci nihil ipsa placeat.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(51, 'Yaiza Sanabria', 'Debitis non dolor excepturi nostrum aut blanditiis impedit.', 'Ducimus iure nam corrupti consequatur modi nisi iusto. Repellat dignissimos sunt cumque aspernatur.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(52, 'Lic. Bruno Nieto', 'Quo aut sed omnis dignissimos sapiente.', 'Est voluptates voluptatem iste molestiae. Deleniti vel soluta veritatis accusantium. Quaerat odit dolores incidunt expedita explicabo repudiandae ex. Commodi dolorem consequuntur maxime sit aut voluptas blanditiis saepe.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(53, 'Ing. Gonzalo Contreras', 'Error sit ea necessitatibus illum.', 'Vero accusamus quam aut sed quos quo. Optio voluptas et nemo aut ducimus. Qui libero ipsum commodi voluptatem. Qui nihil iusto non rem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(54, 'Iria Arellano', 'Eum et sit enim quo.', 'Quibusdam ducimus et sapiente consectetur. Porro impedit ipsa a quis. Praesentium omnis ab sit quo odio sunt.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(55, 'Ing. Marina Centeno Segundo', 'Consequatur repudiandae repellendus iure tempora soluta.', 'Vel beatae quod blanditiis sint. Rem reprehenderit repellat animi aut accusamus tenetur pariatur quo. Illo quia rerum consequuntur doloribus eius debitis nemo.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(56, 'Jose Beltrán Segundo', 'Deleniti at quis aut.', 'Dolores consequuntur eaque quasi dolorem saepe atque. Provident impedit minima recusandae aut magni voluptates nulla architecto. Error ducimus quidem tempora voluptatem ea autem. Cum eum nihil quia fugit quibusdam et quia. Placeat dolores quo iste quam.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(57, 'Eric Puente', 'Ex omnis porro velit.', 'Laudantium alias odit ipsa nobis porro. Repellat sed nulla aut laboriosam provident. Atque dolorem non illo nobis. Asperiores cupiditate ullam quia ad ut accusamus quae.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(58, 'Carmen Godoy', 'Consequatur tenetur autem ut rerum assumenda ut inventore eligendi.', 'Ipsam voluptate minus dolorum et aliquam voluptas. Quasi et voluptas omnis unde. Animi distinctio ab inventore amet cupiditate ea ea est. Voluptatibus explicabo quia aperiam perspiciatis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(59, 'Gael Santacruz', 'Expedita vel maxime dolore id.', 'Consequatur odit provident voluptate minima sint aut distinctio. Rerum nobis enim voluptatum quos dolor nihil.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(60, 'Salma Serra', 'Consequatur modi ullam ipsam aut.', 'Ipsum consequatur sit laborum eum. Quia temporibus voluptatem quibusdam rerum impedit dolorem vitae.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(61, 'Sra. Iria Roig', 'Aspernatur saepe soluta ducimus amet non dolores atque tempore.', 'Rerum a molestias modi dolores. Nihil harum praesentium enim. Et nisi debitis quia.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(62, 'Erika Delacrúz', 'Facilis necessitatibus inventore et sapiente.', 'Nam eveniet qui iure ea. Commodi aut assumenda aspernatur nulla ad dolorum et cupiditate. Magni ullam perspiciatis vero et explicabo explicabo nostrum autem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(63, 'Sra. Teresa Mora', 'Libero cum iusto reiciendis laboriosam praesentium.', 'In cumque et ea ipsum alias. Modi modi itaque autem aut reprehenderit eos quidem. Ullam quibusdam et qui.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(64, 'Dr. Fatima Ruiz Hijo', 'Asperiores eos sit aut debitis dolorem.', 'Libero non fuga corporis numquam nam consequatur. Velit illo inventore earum excepturi dolore nostrum est. Quas inventore modi debitis at. Velit quas placeat ut voluptatem voluptatem dolores qui dolor. Sunt esse molestiae optio quia fugiat odit.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(65, 'Ing. Alonso Alba', 'Itaque ex sed ad enim distinctio corporis.', 'Accusantium exercitationem ipsum laboriosam odit laborum nisi doloribus. Nisi occaecati quia est.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(66, 'Ainara Negrón', 'Aut rerum tempora eveniet id.', 'Dolores beatae quia molestias voluptate qui sint non. Voluptate voluptatem dicta numquam voluptas nostrum aliquid iure. Omnis suscipit dignissimos quo et vitae et. Laboriosam nemo amet aut corrupti est. Rerum consequatur inventore dicta eveniet dolor.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(67, 'Dr. Jordi Barraza Segundo', 'Ducimus et omnis debitis illo qui.', 'Molestiae animi in vel optio aliquam omnis. Quia nulla impedit natus omnis soluta alias dolores aut. Est itaque quos ea beatae temporibus accusantium.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(68, 'Africa Berríos', 'Ut consectetur omnis quo dolor quae.', 'Voluptatum libero et corporis mollitia et dolores accusantium. Doloremque sapiente et facilis voluptatem omnis necessitatibus. Qui quibusdam sapiente consequuntur magni aut molestias sequi. Dolorem officia in maxime distinctio est qui aut voluptatum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(69, 'Jon Alejandro', 'Nulla deserunt repellat vel.', 'Nisi quaerat minima minima temporibus. Explicabo tempora soluta sunt. Deserunt occaecati quas consectetur illo magnam quas. Corrupti in corrupti provident porro cum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(70, 'Asier Escribano', 'Qui repudiandae earum eius dolore.', 'Voluptatem corrupti ducimus non dolorem ipsa odit. Voluptas eum aut rerum voluptatem inventore.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(71, 'Aya Bustamante', 'Aliquid dicta dolore deleniti et.', 'Amet magni id adipisci ex. Quasi quia ipsum distinctio id occaecati sapiente vero.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(72, 'Isabel Martí', 'Maiores laudantium corrupti et.', 'Expedita est harum necessitatibus aut non voluptate. A vel aut ipsum aut ut aut est. Aut repudiandae facilis dolor accusamus nostrum est similique voluptate.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(73, 'Pol Hurtado', 'Beatae ab et at aut veniam a facere velit.', 'Alias voluptatibus amet ea assumenda dolorem quas et. Laboriosam voluptatem sit enim aut. Illum iusto quo molestias qui et in.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(74, 'Claudia Gil', 'Voluptas laudantium vitae qui iusto aut sit.', 'Sit distinctio aut repellat eius voluptatibus quae. Est necessitatibus quis sint sunt sequi quas. Voluptatem accusantium sapiente vel dolor fuga ut.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(75, 'Cesar Giménez', 'Qui aut cupiditate earum sunt rerum.', 'Voluptas enim et repudiandae est velit rerum. Architecto in eum impedit voluptatem autem. Cum corrupti qui ut id ratione nam. Mollitia ex et ut provident occaecati tempore est.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(76, 'Roberto Feliciano', 'Quibusdam ipsum at tempore vitae ut.', 'Aut dolore omnis commodi excepturi. Nobis ad dolorum molestiae qui earum est. Dolore et culpa sunt rerum.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(77, 'Dr. Diego Santamaría', 'Facilis quam id vel qui.', 'Omnis veritatis ad fugit excepturi at. A numquam autem sint vel. Dolor vel quia ea occaecati explicabo.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(78, 'Dr. Alexandra Vela Tercero', 'Hic porro laudantium soluta doloribus placeat eveniet quia.', 'Ab laudantium ducimus autem commodi. Cum possimus dolor est est ipsum omnis voluptatem. Labore eaque consequuntur molestiae enim facilis laboriosam nisi. Eos ratione consequatur molestiae ducimus.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(79, 'Sra. Berta Armas', 'Autem omnis sed eveniet labore ipsa voluptatibus et.', 'Sed velit distinctio explicabo sunt. Eveniet eos enim odio accusamus explicabo. Architecto nulla qui recusandae voluptatem.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(80, 'Srita. Carolina Saldaña Segundo', 'Cumque vel et distinctio non.', 'Libero sed porro omnis labore sit repellendus omnis. Quas fugiat voluptas aut. Magnam ut et enim cupiditate aliquam sit labore.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(81, 'Ing. Diana Montenegro', 'Voluptatem itaque aliquam cumque iusto et.', 'Et labore repellat accusamus ipsum itaque eos nemo. Voluptatem quia asperiores magnam harum. Qui explicabo similique non impedit asperiores.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(82, 'Dn. Guillem Reyna', 'Molestiae ut est ut quia.', 'Sequi quibusdam labore alias nemo dolor. Quia aspernatur quos qui. Quis et sint natus. Cum sapiente voluptatem aut et.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(83, 'Sra. Paola Sanz Tercero', 'Vel aliquid id blanditiis.', 'Quia praesentium commodi aut omnis. Et voluptatem et et nemo. Necessitatibus aut sit et ut et ex id beatae. Rerum aliquam tempora enim voluptatem officia qui distinctio. Quas adipisci animi tempora est dolores atque.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(84, 'Lic. Biel Acosta', 'Aperiam atque est omnis magni sed enim expedita.', 'Maxime voluptas debitis minus molestiae. Voluptatem rerum ipsam accusantium aut accusamus perferendis. Magnam ducimus officia error ut. Ut fugiat exercitationem voluptate. Velit beatae nemo nam delectus saepe.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(85, 'Lic. Marina Guzmán Tercero', 'Excepturi dicta ut laboriosam provident dolor.', 'Cumque ab velit odit culpa. Officia necessitatibus similique rerum atque. Dolorem deleniti repellendus aut qui occaecati dolor.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(86, 'Yaiza Amaya', 'Est quo optio voluptatem.', 'Eum exercitationem quos et molestias minus et dolor. Saepe magnam dolores voluptatem provident praesentium et. Voluptas dolores sunt ipsa ut.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(87, 'Marta Pedroza', 'Iusto et aspernatur et quia eum voluptatum non.', 'Repellendus molestiae quia minima qui voluptatibus. Perspiciatis dolores et reprehenderit voluptatum pariatur quibusdam. Cum est corporis aperiam. Voluptatem blanditiis corrupti quis omnis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(88, 'Leyre Benito', 'Deserunt blanditiis nihil aut esse iusto qui.', 'Provident dolor aliquam laudantium blanditiis consectetur quibusdam magnam ipsa. Nemo et inventore ab eos earum quae. Voluptatum magnam est nihil ratione omnis quisquam beatae. Quidem earum est odit eaque nostrum aut.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(89, 'Erik Salcido', 'Natus qui tenetur expedita earum iste omnis.', 'Excepturi officiis beatae quis voluptate qui molestias sit. Aut qui voluptas dolor qui cumque cum dignissimos.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(90, 'Adriana Lázaro', 'Eveniet minus eos reprehenderit reprehenderit totam ut.', 'Culpa suscipit accusamus sequi enim accusantium rem nemo numquam. Assumenda saepe aut repudiandae nam eveniet praesentium. Dolor vitae aperiam sed eveniet id.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(91, 'Nil Espinal', 'Illo et impedit id in.', 'Repellendus unde ea aliquam omnis delectus et. Nobis quas temporibus repudiandae ea. Eius amet facere nihil ut.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(92, 'Lic. Raquel Murillo Hijo', 'Mollitia vel iusto delectus laboriosam.', 'Expedita est vel assumenda id sunt voluptate. Deserunt sint qui qui dolor et rerum. Cum debitis id est cupiditate sed repudiandae architecto.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(93, 'Natalia Peña', 'Delectus nemo odit iste et dolores deserunt praesentium.', 'Repellendus rerum dolores voluptatem praesentium eum sint itaque. Sapiente et amet libero ipsum illo qui. Consequatur excepturi ducimus nam cupiditate neque rerum qui.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(94, 'Marcos Tejeda', 'Voluptate est in eos et veritatis commodi eos.', 'Tempora impedit porro ipsum ut. Sapiente ea et eius quis. Architecto nam repellat repellendus eos nesciunt tempore non consequatur. Et ea laudantium sapiente et asperiores.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(95, 'Dn. Marc Tórrez Segundo', 'Soluta quia quae et perferendis et.', 'Voluptatem delectus quia et illo. Corporis repudiandae magnam natus rerum et. Voluptates voluptatem culpa a non consectetur at facilis.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(96, 'Saul Andrés', 'Qui nihil quos porro reiciendis sunt et.', 'Odio adipisci similique nostrum qui voluptatem quam qui. Quaerat nesciunt facere voluptatem atque sapiente recusandae assumenda. Voluptas voluptatibus fugiat nesciunt qui dignissimos. Aut culpa quod ea id placeat.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(97, 'Dn. Iker Enríquez Tercero', 'Iure non nulla deleniti.', 'Consequatur dolores porro quidem quas. Eveniet sit est ut molestias modi totam fugit illo. Quae id nemo ipsam recusandae praesentium. Sed et eum odio ex debitis et eius.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(98, 'Hugo Bonilla', 'Voluptatem veritatis numquam et eos.', 'Qui dolorem placeat voluptas nesciunt voluptate in sunt odit. Nemo eius voluptates dolorem aut veniam eos.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(99, 'Ing. Joel Aguayo Segundo', 'Necessitatibus itaque hic dolores.', 'Aut est rem est. Est sint dolor assumenda velit. Et voluptatibus quia modi voluptatem. Est cum ex sed dolorum esse.', '2017-02-26 22:11:18', '2017-02-26 22:11:18'),
(100, 'Santiago Nieto', 'Aut exercitationem architecto blanditiis est rerum iusto.', 'Quibusdam consequuntur quae ducimus. Culpa pariatur qui atque doloribus qui optio. Nesciunt quo voluptates ut autem maiores consectetur. Explicabo animi eum et ducimus ullam nam.', '2017-02-26 22:11:18', '2017-02-26 22:11:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
